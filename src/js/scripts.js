"use strict";

window.addEventListener("DOMContentLoaded", init);

const HTML = {};
const datasponsors = "sponsors.json";

function init() {
  HTML.burgerMenu = document.querySelector("nav > #burger-menu");
  HTML.menuList = document.querySelector("#menu_list");
  HTML.listOfArticles = document.querySelector("#article_list");
  HTML.pages = document.querySelectorAll("#pages > a");
  HTML.dataEndpoint =
    "https://frontendspring20-e4cd.restdb.io/rest/html24s-articles-test?idtolink=true";
  HTML.apiKey = "5e956ffd436377171a0c230f";
  HTML.searchfield = document.querySelector("input[type='search']");
  HTML.sliderImgs = document.querySelectorAll(".slider_container > .slide_img");
  HTML.myIndex = Math.floor(Math.random() * HTML.sliderImgs.length);
  HTML.randomI = new Array();
  HTML.page1 = document.querySelector(".page1");
  HTML.page2 = document.querySelector(".page2");
  HTML.page3 = document.querySelector(".page3");
  HTML.page4 = document.querySelector(".page4");
  HTML.page5 = document.querySelector(".page5");
  HTML.currentPage = document.querySelector(".current");

  HTML.burgerMenu.addEventListener("click", toggleMenu);
  HTML.searchfield.addEventListener("keyup", searching);
  shuffleSplashImgs();
  setupSlider();
  getData();
}
function shuffleSplashImgs() {
  for (let i = 0; i < HTML.sliderImgs.length; i++) {
    HTML.randomI.push(i);
  }

  //http://stackoverflow.com/a/18650169/1582080
  HTML.randomI.sort(function () {
    return 0.5 - Math.random();
  });
}

function setupSlider() {
  for (let i = 0; i < HTML.sliderImgs.length; i++) {
    HTML.sliderImgs[i].classList.add("hidden");
  }
  HTML.myIndex++;
  if (HTML.myIndex > HTML.sliderImgs.length) {
    HTML.myIndex = 1;
  }
  HTML.sliderImgs[HTML.randomI[HTML.myIndex - 1]].classList.remove("hidden");
  setTimeout(setupSlider, 3000); // Change image every 3 seconds
}
function toggleMenu() {
  console.log("toggle");
  HTML.menuList.classList.toggle("hidden");
  HTML.closedNav = HTML.menuList.classList.contains("hidden");
  if (!HTML.closedNav) {
    HTML.burgerMenu.classList.add("change");
  } else {
    HTML.burgerMenu.classList.remove("change");
  }
}
function getData() {
  HTML.listOfArticles.innerHTML = "";
  fetch(HTML.dataEndpoint, {
    method: "get",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "x-apikey": HTML.apiKey,
      "cache-control": "no-cache",
    },
  })
    .then((data) => data.json())
    .then((data) => {
      HTML.totalArticles = data.length;
      showData(data);
    });
}
function showData(data) {
  HTML.first3 = data.slice(0, 3);
  HTML.next3Page2 = data.slice(3, 6);
  HTML.next3Page3 = data.slice(6, 9);
  HTML.next3Page4 = data.slice(9, 12);
  HTML.last3 = data.slice(12, 15);
  HTML.allPages = document.querySelectorAll(".page1, .page2, .page3");
  if (HTML.page1.classList.contains("active")) {
    HTML.first3.forEach(showArticles);
  }
  HTML.page1.addEventListener("click", () => {
    HTML.page1.classList.add("active");
    HTML.page2.classList.remove("active");
    HTML.page3.classList.remove("active");
    HTML.page4.classList.remove("active");
    HTML.page5.classList.remove("active");
    clearList();
    console.log("hello");
    HTML.first3.forEach(showArticles);
    HTML.currentPage.textContent = "Page 1 of 3";
  });
  HTML.page2.addEventListener("click", () => {
    HTML.page2.classList.add("active");
    HTML.page1.classList.remove("active");
    HTML.page3.classList.remove("active");
    HTML.page4.classList.remove("active");
    HTML.page5.classList.remove("active");
    clearList();
    HTML.next3Page2.forEach(showArticles);
    HTML.currentPage.textContent = "Page 2 of 3";
  });
  HTML.page3.addEventListener("click", () => {
    HTML.page3.classList.add("active");
    HTML.page2.classList.remove("active");
    HTML.page1.classList.remove("active");
    HTML.page4.classList.remove("active");
    HTML.page5.classList.remove("active");
    clearList();
    HTML.next3Page3.forEach(showArticles);
    HTML.currentPage.textContent = "Page 3 of 3";
  });
  HTML.page4.addEventListener("click", () => {
    HTML.page4.classList.add("active");
    HTML.page3.classList.remove("active");
    HTML.page2.classList.remove("active");
    HTML.page1.classList.remove("active");
    clearList();
    HTML.next3Page4.forEach(showArticles);
    HTML.currentPage.textContent = "Page 3 of 3";
  });
  HTML.page5.addEventListener("click", () => {
    HTML.page5.classList.add("active");
    HTML.page4.classList.remove("active");
    HTML.page3.classList.remove("active");
    HTML.page2.classList.remove("active");
    HTML.page1.classList.remove("active");
    clearList();
    HTML.last3.forEach(showArticles);
    HTML.currentPage.textContent = "Page 3 of 3";
  });
  document.querySelector(".back").addEventListener("click", () => {
    HTML.currentPage.textContent = "Page 1 of 3";
    HTML.page1.classList.add("active");
    HTML.page2.classList.remove("active");
    HTML.page3.classList.remove("active");
    HTML.page4.classList.remove("active");
    HTML.page5.classList.remove("active");
    clearList();
    HTML.first3.forEach(showArticles);
  });
}
function clearList() {
  document.querySelector("#article_list").innerHTML = "";
}
function showArticles(article) {
  HTML.pattern = document.querySelector("template").content;
  HTML.clone = HTML.pattern.cloneNode(true);
  HTML.clone.querySelector(".picture").src = article.picture_0;
  HTML.clone.querySelector(".heading").textContent = article.heading;
  HTML.clone.querySelector(".text").innerHTML =
    article.text + "<span>...</span>";
  HTML.clone.querySelector(".more").textContent = article.extra;
  HTML.clone
    .querySelectorAll("#oneArticle, .read_more, .more")
    .forEach((elm) => {
      elm.dataset.id = article._id;
    });
  HTML.clone
    .querySelector(`[data-action="show"]`)
    .addEventListener("click", (elm) => {
      showMoreText(article._id);
    });
  HTML.listOfArticles.appendChild(HTML.clone);
}
function showMoreText(id) {
  document.querySelector(`.more[data-id="${id}"]`).classList.toggle("hidden");
  HTML.textHidden = document
    .querySelector(`.more[data-id="${id}"]`)
    .classList.contains("hidden");
  if (!HTML.textHidden) {
    document.querySelector(`.read_more[data-id="${id}"]`).textContent =
      "Read less";
  } else {
    document.querySelector(`.read_more[data-id="${id}"]`).textContent =
      "Read more";
  }
}
function searching(evt) {
  HTML.keywords = evt.target.value;
  HTML.allTexts = document.querySelectorAll("p, q, h1, h2, h3, h4, address");

  for (let i = 0; i < HTML.allTexts.length; i++) {
    HTML.texts = HTML.allTexts[i].innerText;
    HTML.lowerTexts = HTML.texts.toLowerCase();
    HTML.lowerKeywords = HTML.keywords.toLowerCase();

    HTML.index = HTML.lowerTexts.indexOf(HTML.lowerKeywords);
    HTML.key = HTML.texts.substring(HTML.index, HTML.texts.length);
    if (HTML.index > -1 || HTML.lowerTexts === HTML.lowerKeywords) {
      HTML.results =
        HTML.texts.substring(0, HTML.index) +
        "<mark>" +
        HTML.texts.substring(HTML.index, HTML.index + HTML.texts.length) +
        "</mark>" +
        HTML.texts.substring(HTML.index + HTML.texts.length);
      HTML.allTexts[i].innerHTML = HTML.results;
    }
  }

  if (HTML.keywords === "") {
    HTML.marks = document.querySelectorAll("mark");
    HTML.marks.forEach((m) => {
      m.style.backgroundColor = "transparent";
      m.style.color = "unset";
    });
  }
}
